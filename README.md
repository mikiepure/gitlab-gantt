# gitlab-gantt

gitlab-gantt is an application software to create and show gantt-chart diagram from GitLab milestones and issues.

## Setup

You can use 2 kinds of HTML:

- (1) `index.html`: using online JavaScript and CSS via CDN.
- (2) `index-local.html`: using local JavaScript and CSS installed as node modules.

In case of (1):

1. Clone the repository.
2. Open `index.html` by a web browser.

In case of (2):

1. Install Node.js and yarn package in the PC.
2. Clone the repository.
3. Execute `yarn install` command in the console.
4. Open `index-local.html` by a web browser.

## Usage

1. Input Base URL, Project ID, and Private Token.
2. Press Load button.
