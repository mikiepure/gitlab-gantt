/* global $, moment, angular */

const CONFIG = {
  colors: {
    title: {
      closed: "#808080",
      expired: "#DB3B21",
      open: "#000000"
    },
    label: {
      Doing: "#5CB85C",
      "To Do": "#F0AD4E"
    },
    chart: {
      issue: {
        closed: "#C0C0C0",
        ongoing: "#A8D9A8",
        unstarted: "#F5C98B"
      },
      milestone: {
        closed: "#C0C0C0",
        expired: "#EF9F92",
        upcoming: "#92C0EF",
        open: "#93EEB7"
      }
    },
    bg: {
      milestone: "#DAD5FF"
    }
  }
};

window.addEventListener("load", function() {
  // create modal dialog
  $("#load-dialog").dialog({
    modal: true,
    autoOpen: false,
    closeOnEscape: false,
    draggable: false,
    resizable: false,
    open: function(event, ui) {
      // hide title bar of the dialog
      $(".ui-dialog-titlebar").hide();

      // disable OK button of the dialog
      const button = $("#dialog-button-ok");
      button.attr("disabled", true);
      button.addClass("ui-state-disabled");
    },
    buttons: [
      {
        id: "dialog-button-ok",
        text: "OK",
        click: function() {
          $(this).dialog("close");
        }
      }
    ]
  });

  document.getElementById("switch-display-header-sub").addEventListener(
    "click",
    function() {
      const headerSub = document.getElementById("header-sub");
      if (headerSub.style.display === "none") {
        headerSub.style.display = "block";
      } else {
        headerSub.style.display = "none";
      }
    },
    false
  );
});

let dataProject = null;
let dataMilestones = null;
let dataIssues = null;

// create angular modules
const app = angular.module("App", [
  "gantt",
  "gantt.tree",
  "gantt.table",
  "gantt.tooltips"
]);
app.controller("AppController", function($scope) {
  // initialize ng-models
  $scope.baseUrl = "https://gitlab.com";
  $scope.projectId = "<Project ID>";
  $scope.privateToken = "<Private Token>";
  $scope.milestonesState = "";
  $scope.issuesState = "";

  $scope.data = [];
  $scope.options = {
    columns: ["title", "from", "to", "assignee"],
    headers: {
      title: "Title",
      from: "Start date",
      to: "Due date",
      assignee: "Assignee"
    },
    formatters: {
      from: function(value, column, row) {
        return value.format("YYYY-MM-DD");
      },
      to: function(value, column, row) {
        return value.format("YYYY-MM-DD");
      }
    },
    classes: {
      title: "gantt-column-title",
      from: "gantt-column-from",
      to: "gantt-column-to",
      assignee: "gantt-column-assignee"
    }
  };

  // set width of side table after rendering gantt chart
  // width will be computed automatically based on content
  $scope.registerApi = function(api) {
    api.core.on.rendered($scope, function() {
      api.side.setWidth();
    });
  };

  // set gray as background color for weekends
  $scope.timeFrames = {
    closed: {
      working: false,
      color: "gray",
      classes: ["gantt-closed-timeframe"]
    }
  };
  $scope.dateFrames = {
    weekend: {
      evaluator: function(date) {
        // in case of saturday and sunday,
        return date.isoWeekday() === 6 || date.isoWeekday() === 7;
      },
      targets: ["closed"] // use time-frame named closed
    }
  };

  // event handler: click load button
  $scope.load = async function() {
    // open loading dialog
    $("#load-dialog").html("Loading from GitLab...");
    $("#load-dialog").dialog("open");

    // initialize data
    $scope.data = [];

    const urlApi = `${$scope.baseUrl}/api/v4`;
    const paramToken = `private_token=${$scope.privateToken}`;
    const urlProject = `${urlApi}/projects/${$scope.projectId}`;

    // load project
    dataProject = await GitLabProject.load(urlProject, paramToken);
    if (dataProject === null) {
      $("#load-dialog").html(
        '<span style="color: red">Failed to get project data from GitLab!</span>'
      );
      const button = $("#dialog-button-ok");
      button.attr("disabled", false);
      button.removeClass("ui-state-disabled");
      return;
    }

    // load milestones
    dataMilestones = await loadMilestones(
      `${urlProject}/milestones`,
      paramToken,
      $scope.milestonesState
    );
    if (dataMilestones === null) {
      $("#load-dialog").html(
        '<span style="color: red">Failed to get milestone data from GitLab!</span>'
      );
      const button = $("#dialog-button-ok");
      button.attr("disabled", false);
      button.removeClass("ui-state-disabled");
      return;
    }

    // load issues
    dataIssues = await loadIssues(
      `${urlProject}/issues`,
      paramToken,
      $scope.issuesState
    );
    if (dataIssues === null) {
      $("#load-dialog").html(
        '<span style="color: red">failed to get issue data from GitLab!</span>'
      );
      const button = $("#dialog-button-ok");
      button.attr("disabled", false);
      button.removeClass("ui-state-disabled");
      return;
    }

    // set GitLab data to angular data
    $scope.setData();

    // apply angular data to gantt chart
    $scope.$apply();

    // close loading dialog
    $("#load-dialog").dialog("close");
  };

  $scope.setData = function() {
    // add milestones to data
    if (dataMilestones.length > 0) {
      const milestones = dataMilestones.map(function(milestone) {
        const url = `${dataProject.webUrl}/milestones/${milestone.iid}`;
        return {
          name: `M#${milestone.iid}`,
          color: CONFIG.colors.bg.milestone,
          tasks: [
            {
              name: milestone.title,
              color: getMilestoneColor(milestone),
              from: milestone.start_date,
              to: reviseDueDate(milestone.due_date)
            }
          ],
          content: `<a href="${url}">#${milestone.iid}</a>`,
          columnContents: {
            title: getMilestoneStyledTitle(milestone),
            assignee: ""
          }
        };
      });

      Array.prototype.push.apply($scope.data, milestones);
    }

    // add issues to data
    if (dataIssues.length > 0) {
      const issues = dataIssues.map(function(issue) {
        let tempIssue = {
          name: "I#" + issue.iid,
          tasks: [
            {
              id: issue.title,
              name: issue.title,
              color: getIssueColor(issue),
              from: getIssueStartDate(issue),
              to: getIssueDueDate(issue)
            }
          ],
          content: `<a href="${issue.web_url}">#${issue.iid}</a>`,
          columnContents: {
            title: getIssueStyledTitle(issue),
            assignee: issue.assignee ? issue.assignee.name : "-"
          }
        };
        if (issue.milestone) {
          tempIssue.parent = `M#${issue.milestone.iid}`;
        }
        return tempIssue;
      });

      Array.prototype.push.apply($scope.data, issues);
    }
  };

  $scope.applyWidthStyle = function() {
    if ($scope.titleWidth !== null) {
      $(".gantt-column-title").css("width", Number($scope.titleWidth));
    } else {
      $(".gantt-column-title").css("width", "");
    }
    if ($scope.startDateWidth !== null) {
      $(".gantt-column-from").css("width", Number($scope.startDateWidth));
    } else {
      $(".gantt-column-from").css("width", "");
    }
    if ($scope.dueDateWidth !== null) {
      $(".gantt-column-to").css("width", Number($scope.dueDateWidth));
    } else {
      $(".gantt-column-to").css("width", "");
    }
    if ($scope.assigneeWidth !== null) {
      $(".gantt-column-assignee").css("width", Number($scope.assigneeWidth));
    } else {
      $(".gantt-column-assignee").css("width", "");
    }
    if ($scope.columnWidth !== null) {
      $scope.options.columnWidth = Number($scope.columnWidth);
    } else {
      $scope.options.columnWidth = undefined;
    }
  };
});

// ############################################################################
// # communication to GitLab
// ############################################################################

class GitLabProject {
  /**
   * load data and create GitLabProject object from GitLab
   * @param {string} url - base URL for GitLab
   * @param {string} token - token to access GitLab API
   * @return {GitLabProject} GitLabProject object
   *
   */
  static async load(url, token) {
    const data = await restApiGet(`${url}?${token}`);
    if (data === null) {
      return null;
    }

    return new GitLabProject(data[1]);
  }

  /**
   * constructor
   * @param {Object} data - response data from GitLab Projects API
   *
   */
  constructor(data) {
    this._data = data;
  }

  /**
   * get url of the project
   * @return {string} url of the project
   */
  get webUrl() {
    return this._data.web_url;
  }
}

async function loadMilestones(url, token, state) {
  if (state !== "") {
    state = `&state=${state}`;
  }

  const milestones = [];

  let page = "0";
  while (true) {
    const data = await restApiGet(`${url}?page=${page}&${token}${state}`);
    if (data === null) {
      return null;
    }

    if (data[1].length > 0) {
      Array.prototype.push.apply(milestones, data[1]);
    }

    page = data[0].headers.get("x-next-page");
    if (page === null || page === "") {
      return milestones;
    }
  }
}

async function loadIssues(url, token, state) {
  if (state !== "") {
    state = `&state=${state}`;
  }

  const issues = [];

  let page = "0";
  while (true) {
    const data = await restApiGet(`${url}?page=${page}&${token}${state}`);
    if (data === null) {
      return null;
    }

    if (data[1].length > 0) {
      Array.prototype.push.apply(issues, data[1]);
    }

    page = data[0].headers.get("x-next-page");
    if (page === null || page === "") {
      return issues;
    }
  }
}

// ############################################################################
// # formatter of gantt chart
// ############################################################################

function getIssueStyledTitle(issue) {
  let labelStarColor = CONFIG.colors.title.open;
  if (issue.state === "closed") {
    labelStarColor = CONFIG.colors.title.closed; // Board: Closed
  } else {
    for (const key of Object.keys(CONFIG.colors.label)) {
      if (issue.labels.indexOf(key) !== -1) {
        labelStarColor = CONFIG.colors.label[key];
        break;
      }
    }
  }
  const labelStar = `<span style="color: ${labelStarColor}">&#9733;</span>`;

  let titleColor;
  if (issue.state === "closed") {
    titleColor = CONFIG.colors.title.closed; // Closed
  } else {
    const now = new Date();
    const dueDate = moment(getIssueDueDate(issue)).toDate();
    if (now.getTime() > dueDate.getTime()) {
      titleColor = CONFIG.colors.title.expired; // Expired
    } else {
      titleColor = CONFIG.colors.title.open; // Not Expired
    }
  }
  const title = `<span style="color: ${titleColor}">${issue.title}</span>`;

  return `${labelStar}&nbsp;${title}`;
}

function getMilestoneStyledTitle(milestone) {
  let color = "";

  if (milestone.state === "closed") {
    color = CONFIG.colors.title.closed; // Closed
  } else {
    const now = new Date();
    const dueDate = moment(reviseDueDate(milestone.due_date)).toDate();
    if (now.getTime() > dueDate.getTime()) {
      color = CONFIG.colors.title.expired; // Expired
    } else {
      color = CONFIG.colors.title.open; // Upcoming or Open
    }
  }

  return `<span style="color: ${color}">${milestone.title}</span>`;
}

function getMilestoneColor(milestone) {
  if (milestone.state === "closed") {
    return CONFIG.colors.chart.milestone.closed; // Closed
  }

  const now = new Date();
  const startDate = moment(milestone.start_date).toDate();
  const dueDate = moment(reviseDueDate(milestone.due_date)).toDate();
  if (now.getTime() < startDate.getTime()) {
    return CONFIG.colors.chart.milestone.upcoming; // Upcoming
  } else if (now.getTime() < dueDate.getTime()) {
    return CONFIG.colors.chart.milestone.open; // Open
  }

  return CONFIG.colors.chart.milestone.expired; // Expired
}

function getIssueColor(issue) {
  if (issue.state === "closed") {
    return CONFIG.colors.chart.issue.closed; // Completed (closed)
  }

  if (issue.assignee) {
    return CONFIG.colors.chart.issue.ongoing; // Ongoing (open and assigned)
  }

  return CONFIG.colors.chart.issue.unstarted; // Unstarted (open and unassigned)
}

function getIssueStartDate(issue) {
  if (issue.description) {
    const matches = issue.description.match(
      /Start *date *: *(\d{4}-\d{2}-\d{2})/i
    );
    if (matches) {
      return matches[1];
    }
  }

  if (issue.milestone) {
    return issue.milestone.start_date;
  }

  return "";
}

function getIssueDueDate(issue) {
  if (issue.due_date) {
    return reviseDueDate(issue.due_date);
  }

  if (issue.milestone) {
    return reviseDueDate(issue.milestone.due_date);
  }

  return "";
}

function reviseDueDate(date) {
  return moment(date)
    .add(23, "hour")
    .add(59, "minute");
}

// ############################################################################
// # util
// ############################################################################

async function restApiGet(url) {
  const method = "GET";
  console.debug(`[${method}>] ${url}`);

  try {
    const resp = await fetch(url, { method });
    if (resp.status !== 200) {
      console.error(`[${method}<] ${url}: ${resp.statusText} (${resp.status})`);
      return null;
    }
    console.debug(`[${method}<] ${url}: ${resp.statusText} (${resp.status})`);
    const json = await resp.json();
    console.info(`[${method}] ${url}: {...}`);
    // console.debug(`[${method}] ${url}: ${JSON.stringify(json)}`);
    return [resp, json];
  } catch (e) {
    console.error(`[${method}] ${url}: ${e}`);
    return null;
  }
}
